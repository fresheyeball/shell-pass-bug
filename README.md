## Shell pass through bug

Clone this repo.

```bash
git clone git@gitlab.com:fresheyeball/shell-pass-bug.git
```

Observer the shell hook in `default.nix`

```nix
}).overrideAttrs (old: {
  shellHook = ''
    echo WOWZERS
    alias trousers="echo 'in my trousers'"
  '';
})
```

### Expected behavior

```bash
cd shell-pass-bug
nix-shell
trousers
```

Observe that "WOWZERS" is printed by `nix-shell`.
Observe that "in my trousers" is printed by `trousers`.

### Bug behavior

```bash
cd shell-pass-bug
nd shell
trousers
```

Observe that "WOWZERS" is printed by `nd shell`.
Observe that `trousers` produces an error, which is a bug.
